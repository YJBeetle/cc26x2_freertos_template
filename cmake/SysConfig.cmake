# SysConfig
if(CMAKE_HOST_WIN32)
    set(SHELL_SCRIPT_SUFFIX "bat")
else()
    set(SHELL_SCRIPT_SUFFIX "sh")
endif()
set(SYSCONFIG_GENERATED_SRC_LIST
    "${CMAKE_BINARY_DIR}/syscfg/ti_ble_config.h"
    "${CMAKE_BINARY_DIR}/syscfg/ti_ble_config.c"
    "${CMAKE_BINARY_DIR}/syscfg/ti_build_config.opt"
    "${CMAKE_BINARY_DIR}/syscfg/ti_ble_app_config.opt"
    "${CMAKE_BINARY_DIR}/syscfg/ti_devices_config.c"
    "${CMAKE_BINARY_DIR}/syscfg/ti_radio_config.c"
    "${CMAKE_BINARY_DIR}/syscfg/ti_radio_config.h"
    "${CMAKE_BINARY_DIR}/syscfg/ti_drivers_config.c"
    "${CMAKE_BINARY_DIR}/syscfg/ti_drivers_config.h"
)
set(SYSCONFIG_GENERATED_FILES
    ${SYSCONFIG_GENERATED_SRC_LIST}
    "${CMAKE_BINARY_DIR}/syscfg/ti_utils_build_linker.cmd.genlibs"
    "${CMAKE_BINARY_DIR}/syscfg/syscfg_c.rov.xs"
    "${CMAKE_BINARY_DIR}/syscfg/ti_utils_runtime_model.gv"
    "${CMAKE_BINARY_DIR}/syscfg/ti_utils_runtime_Makefile"
)
add_custom_command(
    OUTPUT ${SYSCONFIG_GENERATED_FILES}
    COMMAND ${CMAKE_COMMAND} -E rm -f ${SYSCONFIG_GENERATED_FILES}
    COMMAND ${SYSCONFIG_TOOL_DIR}/sysconfig_cli.${SHELL_SCRIPT_SUFFIX}
        -s "${SDK_INSTALL_DIR}/.metadata/product.json"
        -o "${CMAKE_BINARY_DIR}/syscfg"
        --compiler gcc
        "${SYSCFG_FILE}"
    DEPENDS "${SYSCFG_FILE}"
)
add_library(SysConfig INTERFACE ${SYSCONFIG_GENERATED_FILES})
target_compile_options(SysConfig INTERFACE
    "@${CMAKE_BINARY_DIR}/syscfg/ti_ble_app_config.opt"
    "@${CMAKE_BINARY_DIR}/syscfg/ti_build_config.opt"
)
target_include_directories(SysConfig INTERFACE "${CMAKE_BINARY_DIR}/syscfg")
