# Interface Ti
add_library(Ti INTERFACE)
target_compile_options(Ti INTERFACE
    "@${SDK_INSTALL_DIR}/source/ti/ble5stack/config/build_components.opt"
    "@${SDK_INSTALL_DIR}/source/ti/ble5stack/config/factory_config.opt"
    -mcpu=cortex-m4 -march=armv7e-m -mthumb -mfloat-abi=hard -mfpu=fpv4-sp-d16
    -ffunction-sections -fdata-sections -Wall -Wno-comment
)
target_compile_definitions(Ti INTERFACE
    FLASH_ROM_BUILD=2
    DeviceFamily_CC26X2
    FREERTOS
    NVOCMP_POSIX_MUTEX
)
target_include_directories(Ti INTERFACE
    "${SDK_INSTALL_DIR}/source"
    "${SDK_INSTALL_DIR}/source/ti"
    "${SDK_INSTALL_DIR}/source/ti/ble5stack"
    "${SDK_INSTALL_DIR}/source/ti/ble5stack/controller/cc26xx/inc"
    "${SDK_INSTALL_DIR}/source/ti/ble5stack/inc"
    "${SDK_INSTALL_DIR}/source/ti/ble5stack/rom"
    "${SDK_INSTALL_DIR}/source/ti/ble5stack/common/cc26xx"
    "${SDK_INSTALL_DIR}/source/ti/ble5stack/common/cc26xx/freertos"
    "${SDK_INSTALL_DIR}/source/ti/ble5stack/icall/inc"
    "${SDK_INSTALL_DIR}/source/ti/ble5stack/hal/src/target/_common"
    "${SDK_INSTALL_DIR}/source/ti/ble5stack/hal/src/target/_common/cc26xx"
    "${SDK_INSTALL_DIR}/source/ti/ble5stack/hal/src/inc"
    "${SDK_INSTALL_DIR}/source/ti/ble5stack/heapmgr"
    "${SDK_INSTALL_DIR}/source/ti/ble5stack/profiles/dev_info"
    "${SDK_INSTALL_DIR}/source/ti/ble5stack/profiles/simple_profile"
    "${SDK_INSTALL_DIR}/source/ti/ble5stack/icall/src/inc"
    "${SDK_INSTALL_DIR}/source/ti/ble5stack/osal/src/inc"
    "${SDK_INSTALL_DIR}/source/ti/ble5stack/services/src/saddr"
    "${SDK_INSTALL_DIR}/source/ti/ble5stack/services/src/sdata"
    "${SDK_INSTALL_DIR}/source/ti/common/nv"
    "${SDK_INSTALL_DIR}/source/ti/common/cc26xx"
    "${SDK_INSTALL_DIR}/source/ti/ble5stack/icall/src"
    "${SDK_INSTALL_DIR}/source/ti/ble5stack/profiles/simple_profile/cc26xx"
    "${SDK_INSTALL_DIR}/source/ti/devices/cc13x2_cc26x2"
    "${SDK_INSTALL_DIR}/source/ti/posix/gcc"
    "${FREERTOS_INSTALL_DIR}/FreeRTOS/Source/include"
    "${FREERTOS_INSTALL_DIR}/FreeRTOS/Source/portable/GCC/ARM_CM4F"
)
target_link_directories(Ti INTERFACE "${SDK_INSTALL_DIR}/source")
target_link_options(Ti INTERFACE
    -mfpu=fpv4-sp-d16
    -ffunction-sections -fdata-sections
    -Wall -Wno-comment
    -mcpu=cortex-m4
    -Wl,-Map,${PROJECT_NAME}.map
    -nostartfiles -static -Wl,--gc-sections
    -march=armv7e-m -mthumb -mfloat-abi=hard -mfpu=fpv4-sp-d16 --specs=nano.specs -nostdlib
    "-Wl,-T${LINKER_SCRIPT}"
)
target_link_libraries(Ti INTERFACE
    gcc c m nosys
)

# Ti source
set(TI_SRC_LIST
    "${SDK_INSTALL_DIR}/source/ti/ble5stack/common/cc26xx/freertos/TI_heap_wrapper.c"
    "${SDK_INSTALL_DIR}/source/ti/ble5stack/common/cc26xx/freertos/bget.c"
    "${SDK_INSTALL_DIR}/source/ti/ble5stack/common/cc26xx/freertos/icall_POSIX.c"
    "${SDK_INSTALL_DIR}/source/ti/ble5stack/common/cc26xx/menu/two_btn_menu.c"
    "${SDK_INSTALL_DIR}/source/ti/ble5stack/common/cc26xx/util.c"
    "${SDK_INSTALL_DIR}/source/ti/ble5stack/common/cc26xx/board_key.c"
    "${SDK_INSTALL_DIR}/source/ti/ble5stack/icall/app/icall_api_lite.c"
    "${SDK_INSTALL_DIR}/source/ti/ble5stack/icall/app/ble_user_config.c"
    "${SDK_INSTALL_DIR}/source/ti/ble5stack/icall/src/icall_cc2650.c"
    "${SDK_INSTALL_DIR}/source/ti/ble5stack/icall/src/icall_user_config.c"
    "${SDK_INSTALL_DIR}/source/ti/ble5stack/icall/stack/ble_user_config_stack.c"
    "${SDK_INSTALL_DIR}/source/ti/ble5stack/profiles/dev_info/cc26xx/devinfoservice.c"
    "${SDK_INSTALL_DIR}/source/ti/ble5stack/profiles/simple_profile/cc26xx/simple_gatt_profile.c"
    "${SDK_INSTALL_DIR}/source/ti/ble5stack/rom/agama_r1/rom_init.c"
    "${SDK_INSTALL_DIR}/source/ti/common/nv/crc.c"
    "${SDK_INSTALL_DIR}/source/ti/common/nv/nvocmp.c"
    "${SDK_INSTALL_DIR}/source/ti/drivers/rf/RFCC26X2_multiMode.c"
    ${OSAL_ICALL_BLE}
)
