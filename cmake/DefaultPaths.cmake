# Default paths
if(CMAKE_HOST_APPLE)
    set(TI_INSTALL_DIR_DEF  "/Applications/ti")
elseif(CMAKE_HOST_WIN32)
    set(TI_INSTALL_DIR_DEF  "C:/Program Files/ti")
else()
    set(TI_INSTALL_DIR_DEF  "/opt/ti")
endif()
set(TI_INSTALL_DIR          "${TI_INSTALL_DIR_DEF}" CACHE STRING "TI software installation directory")
set(FREERTOS_INSTALL_DIR    "${TI_INSTALL_DIR}/FreeRTOSv202112.00" CACHE STRING "FreeRTOS installation directory")
set(TOOLCHAIN_PREFIX        "" CACHE STRING "Compiler toolchain path prefix")
set(CCS_DIR_NAME            "ccs1100" CACHE STRING "CCS Folder Name")
if(CMAKE_HOST_WIN32)
    set(SDK_INSTALL_DIR     "${TI_INSTALL_DIR}/${CCS_DIR_NAME}/simplelink_cc13xx_cc26xx_sdk_6_10_00_29" CACHE STRING "SDK installation directory")
    set(XDC_TOOL_DIR        "${TI_INSTALL_DIR}/${CCS_DIR_NAME}/xdctools_3_62_01_15_core" CACHE STRING "XDC installation directory")
    set(SYSCONFIG_TOOL_DIR  "${TI_INSTALL_DIR}/${CCS_DIR_NAME}/sysconfig_1_12_1" CACHE STRING "SysConfig installation directory")
else()
    set(SDK_INSTALL_DIR     "${TI_INSTALL_DIR}/simplelink_cc13xx_cc26xx_sdk_6_10_00_29" CACHE STRING "SDK installation directory")
    set(XDC_TOOL_DIR        "${TI_INSTALL_DIR}/xdctools_3_62_01_15_core" CACHE STRING "XDC installation directory")
    set(SYSCONFIG_TOOL_DIR  "${TI_INSTALL_DIR}/sysconfig_1.12.1" CACHE STRING "SysConfig installation directory")
endif()

# Check path
if(NOT EXISTS ${TI_INSTALL_DIR})
message(FATAL_ERROR "TI_INSTALL_DIR \"${TI_INSTALL_DIR}\" not found")
endif()
if(NOT EXISTS ${FREERTOS_INSTALL_DIR})
    message(FATAL_ERROR "FREERTOS_INSTALL_DIR \"${FREERTOS_INSTALL_DIR}\" not found")
endif()
if(NOT EXISTS ${SDK_INSTALL_DIR})
    message(FATAL_ERROR "SDK_INSTALL_DIR \"${SDK_INSTALL_DIR}\" not found")
endif()
if(NOT EXISTS ${XDC_TOOL_DIR})
    message(FATAL_ERROR "XDC_TOOL_DIR \"${XDC_TOOL_DIR}\" not found")
endif()
if(NOT EXISTS ${SYSCONFIG_TOOL_DIR})
    message(FATAL_ERROR "SYSCONFIG_TOOL_DIR \"${SYSCONFIG_TOOL_DIR}\" not found")
endif()
