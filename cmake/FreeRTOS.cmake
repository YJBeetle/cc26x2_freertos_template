# FreeRTOS
set(FREERTOS_SRC_LIST
    "${FREERTOS_INSTALL_DIR}/FreeRTOS/Source/croutine.c"
    "${FREERTOS_INSTALL_DIR}/FreeRTOS/Source/event_groups.c"
    "${FREERTOS_INSTALL_DIR}/FreeRTOS/Source/list.c"
    "${FREERTOS_INSTALL_DIR}/FreeRTOS/Source/queue.c"
    "${FREERTOS_INSTALL_DIR}/FreeRTOS/Source/stream_buffer.c"
    "${FREERTOS_INSTALL_DIR}/FreeRTOS/Source/tasks.c"
    "${FREERTOS_INSTALL_DIR}/FreeRTOS/Source/timers.c"
    "${FREERTOS_INSTALL_DIR}/FreeRTOS/Source/portable/GCC/ARM_CM4F/port.c"
    "${FREERTOS_INSTALL_DIR}/FreeRTOS/Source/portable/MemMang/heap_4.c"
    "${SDK_INSTALL_DIR}/source/ti/posix/freertos/clock.c"
    "${SDK_INSTALL_DIR}/source/ti/posix/freertos/memory.c"
    "${SDK_INSTALL_DIR}/source/ti/posix/freertos/mqueue.c"
    "${SDK_INSTALL_DIR}/source/ti/posix/freertos/pthread_barrier.c"
    "${SDK_INSTALL_DIR}/source/ti/posix/freertos/pthread_cond.c"
    "${SDK_INSTALL_DIR}/source/ti/posix/freertos/pthread.c"
    "${SDK_INSTALL_DIR}/source/ti/posix/freertos/pthread_mutex.c"
    "${SDK_INSTALL_DIR}/source/ti/posix/freertos/pthread_rwlock.c"
    "${SDK_INSTALL_DIR}/source/ti/posix/freertos/sched.c"
    "${SDK_INSTALL_DIR}/source/ti/posix/freertos/semaphore.c"
    "${SDK_INSTALL_DIR}/source/ti/posix/freertos/sleep.c"
    "${SDK_INSTALL_DIR}/source/ti/posix/freertos/timer.c"
    "${SDK_INSTALL_DIR}/kernel/freertos/dpl/AppHooks_freertos.c"
    "${SDK_INSTALL_DIR}/kernel/freertos/dpl/ClockPCC26X2_freertos.c"
    "${SDK_INSTALL_DIR}/kernel/freertos/dpl/DebugP_freertos.c"
    "${SDK_INSTALL_DIR}/kernel/freertos/dpl/HwiPCC26X2_freertos.c"
    "${SDK_INSTALL_DIR}/kernel/freertos/dpl/MutexP_freertos.c"
    "${SDK_INSTALL_DIR}/kernel/freertos/dpl/PowerCC26X2_freertos.c"
    "${SDK_INSTALL_DIR}/kernel/freertos/dpl/QueueP_freertos.c"
    "${SDK_INSTALL_DIR}/kernel/freertos/dpl/SemaphoreP_freertos.c"
    "${SDK_INSTALL_DIR}/kernel/freertos/dpl/StaticAllocs_freertos.c"
    "${SDK_INSTALL_DIR}/kernel/freertos/dpl/SwiP_freertos.c"
    "${SDK_INSTALL_DIR}/kernel/freertos/dpl/SystemP_freertos.c"
    "${SDK_INSTALL_DIR}/kernel/freertos/dpl/TimerPCC26XX_freertos.c"
    "${SDK_INSTALL_DIR}/kernel/freertos/startup/startup_cc13x2_cc26x2_gcc.c"
)
add_library(FreeRTOS STATIC ${FREERTOS_SRC_LIST})
set_target_properties(FreeRTOS PROPERTIES
    C_STANDARD 99
    C_EXTENSIONS OFF
)
target_compile_options(FreeRTOS PRIVATE
    -O3 -Wunused -Wunknown-pragmas -g
    -ffunction-sections -fdata-sections -fno-builtin-malloc
    -mcpu=cortex-m4 -mthumb -mabi=aapcs -mfloat-abi=hard -mfpu=fpv4-sp-d16
    --specs=nano.specs
)
target_compile_definitions(FreeRTOS PRIVATE "DeviceFamily_CC13X2_CC26X2")
get_filename_component(FREERTOS_CONFIG_DIRECTORIES "${FREERTOS_CONFIG}" DIRECTORY)
target_include_directories(FreeRTOS PRIVATE
    "${FREERTOS_CONFIG_DIRECTORIES}"
    "${FREERTOS_INSTALL_DIR}/FreeRTOS/Source/include"
    "${FREERTOS_INSTALL_DIR}/FreeRTOS/Source/portable/GCC/ARM_CM4F"
    "${SDK_INSTALL_DIR}/source/ti/posix/gcc"
    "${SDK_INSTALL_DIR}/source"
    "${SDK_INSTALL_DIR}/source/third_party/CMSIS/Include"
)
