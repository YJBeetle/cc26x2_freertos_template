# DS path
if(CMAKE_HOST_WIN32)
    set(DS_DIR "${TI_INSTALL_DIR}/${CCS_DIR_NAME}/ccs/ccs_base/DebugServer" CACHE STRING "DebugServer installation directory")
else()
    set(DS_DIR "${TI_INSTALL_DIR}/${CCS_DIR_NAME}/ccs/ccs_base/DebugServer" CACHE STRING "DebugServer installation directory")
endif()
if(NOT EXISTS ${DS_DIR})
    message(FATAL_ERROR "DS_DIR \"${DS_DIR}\" not found")
endif()

# Flash
add_custom_target(flash VERBATIM COMMAND_EXPAND_LISTS
    COMMAND ${DS_DIR}/bin/DSLite
        flash
        -c "${FLASH_CCXML}"
        -l "${FLASH_SETTINGS}"
        -e -f -v
        ${CMAKE_BINARY_DIR}/${PROJECT_NAME}.out
    DEPENDS ${CMAKE_BINARY_DIR}/${PROJECT_NAME}.out
)
add_custom_target(load VERBATIM COMMAND_EXPAND_LISTS
    COMMAND ${DS_DIR}/bin/DSLite
        load
        -c "${FLASH_CCXML}"
        ${CMAKE_BINARY_DIR}/${PROJECT_NAME}.out
    DEPENDS ${CMAKE_BINARY_DIR}/${PROJECT_NAME}.out
)
