# CC26X2_FreeRTOS_Template

## 下载资源

### GCC 编译器

https://developer.arm.com/tools-and-software/open-source-software/developer-tools/gnu-toolchain/gnu-rm/downloads

#### MacOS

    brew install gcc-arm-embedded

### SDK

https://www.ti.com/tool/download/SIMPLELINK-CC13XX-CC26XX-SDK

### SYSCONFIG

https://www.ti.com/tool/download/SYSCONFIG

### CCS

https://www.ti.com/tool/CCSTUDIO

### Flash 工具

https://dev.ti.com/uniflash/#!/main/cc2652r1f/TIXDS110_Connection/Standalone%20Command%20Line

https://www.ti.com/tool/FLASH-PROGRAMMER

## 修补

### Linux

    sed -i "s|1e4|10000|g" /opt/ti/simplelink_cc13xx_cc26xx_sdk_*/kernel/freertos/dpl/SystemP_freertos.c

### MacOS

    sed -i'.bak' "s/1e4/10000/g" /Applications/ti/simplelink_cc13xx_cc26xx_sdk_*/kernel/freertos/dpl/SystemP_freertos.c
